<?xml version="1.0" encoding="utf-8"?>
<!--
/*
** Copyright 2015, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
-->

<!-- These resources are around just to allow their values to be customized
     for different hardware and product builds.  Do not translate. -->
<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">

    <!-- This string array should be overridden by the device to present a list of network
         attributes.  This is used by the connectivity manager to decide which networks can coexist
         based on the hardware -->
    <!-- An Array of "[Connection name],[ConnectivityManager.TYPE_xxxx],
         [associated radio-type],[priority],[restoral-timer(ms)],[dependencyMet]  -->
    <!-- the 5th element "resore-time" indicates the number of milliseconds to delay
         before automatically restore the default connection.  Set -1 if the connection
         does not require auto-restore. -->
    <!-- the 6th element indicates boot-time dependency-met value. -->
    <string-array translatable="false" name="networkAttributes">
        <item>"wifi,1,1,1,-1,true"</item>
        <item>"mobile,0,0,0,-1,true"</item>
        <item>"mobile_mms,2,0,2,60000,true"</item>
        <item>"mobile_supl,3,0,2,60000,true"</item>
        <item>"mobile_dun,4,0,2,60000,true"</item>
        <item>"mobile_hipri,5,0,3,60000,true"</item>
        <item>"mobile_fota,10,0,2,60000,true"</item>
        <item>"mobile_ims,11,0,2,60000,true"</item>
        <item>"mobile_cbs,12,0,2,60000,true"</item>
        <item>"bluetooth,7,7,2,-1,true"</item>
    </string-array>

    <!-- This string array should be overridden by the device to present a list of radio
         attributes.  This is used by the connectivity manager to decide which networks can coexist
         based on the hardware -->
    <!-- An Array of "[ConnectivityManager connectionType],
                      [# simultaneous connection types]"  -->
    <string-array translatable="false" name="radioAttributes">
        <item>"1,1"</item>
        <item>"0,1"</item>
        <item>"7,1"</item>
        <item>"9,1"</item>
    </string-array>

    <!-- Minimum screen brightness allowed by the power manager. -->
    <integer name="config_screenBrightnessDim">10</integer>

    <!-- Minimum allowable screen brightness to use in a very dark room.
         This value sets the floor for the darkest possible auto-brightness
         adjustment.  It is expected to be somewhat less than the first entry in
         config_autoBrightnessLcdBacklightValues so as to allow the user to have
         some range of adjustment to dim the screen further than usual in very
         dark rooms. The contents of the screen must still be clearly visible
         in darkness (although they may not be visible in a bright room). -->
    <integer name="config_screenBrightnessDark">10</integer>

    <!-- Array of light sensor LUX values to define our levels for auto backlight brightness support.
                  The N entries of this array define N + 1 zones as follows:

         Zone 0:        0 <= LUX < array[0]
         Zone 1:        array[0] <= LUX < array[1]
         ...
         Zone N:        array[N - 1] <= LUX < array[N]
         Zone N + 1:    array[N] <= LUX < infinity

         Must be overridden in platform specific overlays -->
    <integer-array name="config_autoBrightnessLevels">
        <item>50</item>
        <item>100</item>
        <item>150</item>
        <item>200</item>
        <item>800</item>
        <item>1300</item>
        <item>2000</item>
        <item>3000</item>
        <item>4000</item>
        <item>8000</item>
    </integer-array>

    <!-- Array of output values for LCD backlight corresponding to the LUX values
                  in the config_autoBrightnessLevels array.  This array should have size one greater
         than the size of the config_autoBrightnessLevels array.
         This must be overridden in platform specific overlays -->
    <integer-array name="config_autoBrightnessLcdBacklightValues">
        <item>24</item>
        <item>36</item>
        <item>47</item>
        <item>71</item>
        <item>118</item>
        <item>141</item>
        <item>165</item>
        <item>188</item>
        <item>212</item>
        <item>230</item>
        <item>255</item>
    </integer-array>

    <!-- Array of OEM specific USB mode override config.
         OEM can override a certain USB mode depending on ro.bootmode.
         Specify an array of below items to set override rule.
         [bootmode]:[original USB mode]:[USB mode used]-->
    <string-array name="config_oemUsbModeOverride">
        <item>bp-tools:mtp:diag,serial_smd,serial_tty,rmnet,usbnet</item>
        <item>bp-tools:mtp,adb:diag,serial_smd,serial_tty,rmnet,usbnet,adb</item>
        <item>bp-tools:ptp:diag,serial_smd,serial_tty,rmnet,usbnet</item>
        <item>bp-tools:ptp,adb:diag,serial_smd,serial_tty,rmnet,usbnet,adb</item>
        <item>bp-tools:rndis:rndis,diag,serial_smd,serial_tty,rmnet,usbnet</item>
        <item>bp-tools:rndis,adb:rndis,diag,serial_smd,serial_tty,rmnet,usbnet,adb</item>
    </string-array>

    <!-- Vibrator pattern for feedback about a long screen/key press -->
    <integer-array name="config_longPressVibePattern">
        <item>0</item>
        <item>1</item>
        <item>20</item>
        <item>21</item>
    </integer-array>

    <!-- Vibrator pattern for feedback about touching a virtual key -->
    <integer-array name="config_virtualKeyVibePattern">
        <item>18</item>
    </integer-array>

    <!-- Vibrator pattern for a very short but reliable vibration for soft keyboard tap -->
    <integer-array name="config_keyboardTapVibePattern">
        <item>18</item>
    </integer-array>

    <!-- Vibrator pattern for feedback about booting with safe mode disabled -->
    <integer-array name="config_safeModeDisabledVibePattern">
        <item>0</item>
        <item>1</item>
        <item>20</item>
        <item>21</item>
    </integer-array>
    <integer-array name="config_safeModeEnabledVibePattern">
        <item>0</item>
        <item>1</item>
        <item>20</item>
        <item>21</item>
        <item>500</item>
        <item>600</item>
    </integer-array>

    <!-- Vibrator pattern for feedback about booting with safe mode disabled -->
    <integer-array name="config_scrollBarrierVibePattern">
        <item>0</item>
        <item>20</item>
        <item>60</item>
        <item>20</item>
    </integer-array>

    <!-- Vibrator pattern to be used as the default for notifications
         that specify DEFAULT_VIBRATE.
     -->
    <integer-array name="config_defaultNotificationVibePattern">
        <item>0</item>
        <item>350</item>
        <item>250</item>
        <item>350</item>
    </integer-array>

    <!-- Vibrator pattern to be used as the default for notifications
         that do not specify vibration but vibrate anyway because the device
         is in vibrate mode.
     -->
    <integer-array name="config_notificationFallbackVibePattern">
        <item>0</item>
        <item>100</item>
        <item>150</item>
        <item>100</item>
    </integer-array>

    <!-- List of regexpressions describing the interface (if any) that represent tetherable
                  USB interfaces.  If the device doesn't want to support tething over USB this should
         be empty.  An example would be "usb.*" -->
    <string-array translatable="false" name="config_tether_usb_regexs">
        <item>"rndis\\d"</item>
    </string-array>

    <!-- Wifi driver supports batched scan -->
    <bool translatable="false" name="config_wifi_batched_scan_supported">false</bool>

    <!-- Is the notification LED intrusive? Used to decide if there should be a disable option -->
    <bool name="config_intrusiveNotificationLed">true</bool>

    <!-- If this is true, the screen will fade off. -->
    <bool name="config_animateScreenLights">true</bool>

    <!-- Indicate whether closing the lid causes the device to go to sleep and opening
         it causes the device to wake up.
         The default is false. -->
    <bool name="config_lidControlsSleep">true</bool>

    <!-- Whether a software navigation bar should be shown. NOTE: in the future this may be
         autodetected from the Configuration. -->
    <bool name="config_showNavigationBar">true</bool>

    <!-- Minimum screen brightness setting allowed by the power manager.
         The user is forbidden from setting the brightness below this level. -->
    <integer name="config_screenBrightnessSettingMinimum">10</integer>

    <!-- Component name of the geofence services provider. -->
    <!-- <string name="config_geofenceServicesProvider" translatable="false">com.motorola.motgeofencesvc</string> -->

    <!-- Set to true if the wifi display supports compositing content stored
         in gralloc protected buffers.  For this to be true, there must exist
         a protected hardware path for surface flinger to composite and send
         protected buffers to the wifi display video encoder.

         If this flag is false, we advise applications not to use protected
         buffers (if possible) when presenting content to a wifi display because
         the content may be blanked.

         This flag controls whether the {@link Display#FLAG_SUPPORTS_PROTECTED_BUFFERS}
         flag is set for wifi displays.
    -->
    <bool name="config_wifiDisplaySupportsProtectedBuffers">true</bool>

    <!-- Default interface to monitor data use -->
    <string name="config_datause_iface">rmnet0</string>

    <!-- Maximum number of supported users -->
    <integer name="config_multiuserMaximumUsers">4</integer>

    <!-- Whether UI for multi user should be shown -->
    <bool name="config_enableMultiUserUI">true</bool>

    <!-- Indicate whether the SD card is accessible without removing the battery. -->
    <bool name="config_batterySdCardAccessibility">true</bool>

    <!-- ComponentName of a dream to show whenever the system would otherwise have
         gone to sleep.  When the PowerManager is asked to go to sleep, it will instead
         try to start this dream if possible.  The dream should typically call startDozing()
         to put the display into a low power state and allow the application processor
         to be suspended.  When the dream ends, the system will go to sleep as usual.
         Specify the component name or an empty string if none.
         Note that doze dreams are not subject to the same start conditions as ordinary dreams.
         Doze dreams will run whenever the power manager is in a dozing state. -->
    <string name="config_dozeComponent">com.android.systemui/com.android.systemui.doze.DozeService</string>

    <!-- Screen brightness used to dim the screen while dozing in a very low power state.
         May be less than the minimum allowed brightness setting
         that can be set by the user. -->
    <integer name="config_screenBrightnessDoze">17</integer>

    <!-- If true, the doze component is not started until after the screen has been
         turned off and the screen off animation has been performed. -->
    <bool name="config_dozeAfterScreenOff">true</bool>

    <!-- Whether WiFi display is supported by this device.
         There are many prerequisites for this feature to work correctly.
         Here are a few of them:
         * The WiFi radio must support WiFi P2P.
         * The WiFi radio must support concurrent connections to the WiFi display and
           to an access point.
         * The Audio Flinger audio_policy.conf file must specify a rule for the "r_submix"
           remote submix module.  This module is used to record and stream system
           audio output to the WiFi display encoder in the media server.
         * The remote submix module "audio.r_submix.default" must be installed on the device.
         * The device must be provisioned with HDCP keys (for protected content). -->
    <bool name="config_enableWifiDisplay">true</bool>

    <!-- Device configuration setting the minfree tunable in the lowmemorykiller in the
         kernel. A high value will cause the lowmemorykiller to fire earlier, keeping more
         memory in the file cache and preventing I/O thrashing, but allowing fewer processes
         to stay in memory. A low value will keep more processes in memory but may cause
         thrashing if set too low. Overrides the default value chosen by ActivityManager based
         on screen size and total memory for the largest lowmemorykiller bucket, and scaled
         proportionally to the smaller buckets. -1 keeps the default. -->
    <integer name="config_lowMemoryKillerMinFreeKbytesAbsolute">81920</integer>

    <!-- The list of components which should be automatically disabled. -->
    <string-array name="config_disabledComponents" translatable="false">
        <item>com.google.android.gsf/com.google.android.gsf.update.SystemUpdateActivity</item>
        <item>com.google.android.gsf/com.google.android.gsf.update.SystemUpdateService$Receiver</item>
        <item>com.google.android.gsf/com.google.android.gsf.update.SystemUpdateService$SecretCodeReceiver</item>
        <item>com.google.android.gms/com.google.android.gms.update.SystemUpdateActivity</item>
        <item>com.google.android.gms/com.google.android.gms.update.SystemUpdateService$Receiver</item>
        <item>com.google.android.gms/com.google.android.gms.update.SystemUpdateService$ActiveReceiver</item>
        <item>com.google.android.gms/com.google.android.gms.update.SystemUpdateService$SecretCodeReceiver</item>
    </string-array>

    <!-- enable doze powersaving mode -->
    <bool name="config_enableAutoPowerModes">true</bool>

   <!-- The RadioAccessFamilies supported by the device.
         Empty is viewed as "all".  Only used on devices which
         don't support RIL_REQUEST_GET_RADIO_CAPABILITY
         format is UMTS|LTE|... -->
    <string translatable="false" name="config_radio_access_family">GSM | WCDMA</string>

    <!-- Older sensors are not setting event.timestamp correctly.  Setting to
          true will use SystemClock.elapsedRealtimeNanos() to set timestamp-->
    <bool name="config_useSystemClockforSensors">true</bool>
</resources>
